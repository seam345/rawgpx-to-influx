{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell rec {
  name = "rust-env";
  buildInputs = [
    pkgs.rustup
    # Add some extra dependencies from `pkgs`
    pkgs.linuxPackages.perf
    pkgs.curl
    pkgs.gnupg

    # helps with testing
    pkgs.cargo-insta
    
    # Add some extra dependencies from `pkgs`
    pkgs.openssl # for sha2 digest
    # for block-utils, to mount the device with the salt on
    pkgs.systemd
    pkgs.pkg-config

    # spell checker
    pkgs.typos

        #pkgs.xorg.libX11
        #pkgs.xorg.libX11.dev
        #pkgs.xorg.libXcursor
        #pkgs.xorg.libxcb
        #pkgs.xorg.libXi
        #pkgs.libxkbcommon
        #pkgs.libxkbcommon.dev
        #pkgs.xorg.libX11.dev
            #pkgs.xorg.libXrandr

  ];
  nativeBuildInputs = [
    #pkgs.pkg-config
      #pkgs.libxkbcommon
      #pkgs.libxkbcommon.dev
   ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;

  INFLUXDB_HOST="http://100.108.38.15:8086";
  INFLUXDB_BUCKET="personal";
  INFLUXDB_ORG="sean";
#programs.nix-ld.enable = true;
#  LD_LIBRARY_PATH="$NIX_LD_LIBRARY_PATH";
#  LD_LIBRARY_PATH = "${lib.makeLibraryPath xorg.libX11}";

#  runtimeDependenciesPath = lib.makeLibraryPath [
#      pkgs.xorg.libX11
#    ];
 #shellHook = ''
    #export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${builtins.toString (pkgs.lib.makeLibraryPath buildInputs)}";
  #'';
}


