use chrono::{DateTime, FixedOffset};
use geo_types::Point;
use influxdb2::models::Query;
use influxdb2::Client;
use influxdb2_derive::{FromDataPoint, WriteDataPoint};
use log::{debug, info};
use std::fs::File;
use std::sync::Arc;
use tokio::sync::Mutex;

use futures::stream;
use geo::GeodesicDistance;
use serde::Deserialize;
use serde::Serialize;
use std::io::{Read, Write};
use tokio::task::JoinHandle;

#[derive(Default, Debug, WriteDataPoint, Serialize, Deserialize)]
#[measurement = "rawgps"]
pub struct InfluxWritableGpsPoint {
    #[influxdb(tag)]
    recording_app: String,
    #[influxdb(tag)]
    recording_device: String,
    #[influxdb(tag)]
    s2_cell_id: String,
    #[influxdb(field)]
    lat: f64,
    #[influxdb(field)]
    lon: f64,
    #[influxdb(field)]
    ele: f64,
    #[influxdb(timestamp)]
    time: i64,
}

#[derive(Default, Debug, FromDataPoint, Serialize, Deserialize)]
pub struct InfluxReadableGpsPoint {
    // #[influxdb(tag)]
    pub(crate) recording_app: String,
    // #[influxdb(tag)]
    pub(crate) recording_device: String,
    // #[influxdb(tag)]
    pub(crate) s2_cell_id: String,
    // #[influxdb(field)]
    pub(crate) lat: f64,
    // #[influxdb(field)]
    pub(crate) lon: f64,
    // #[influxdb(field)]
    pub(crate) ele: f64,
    // #[influxdb(timestamp)]
    pub(crate) time: DateTime<FixedOffset>,
}

impl Into<geo::Point<f64>> for &InfluxReadableGpsPoint {
    fn into(self) -> geo::Point<f64> {
        geo::Point::new(self.lat, self.lon)
    }
}

// todo use sub args instead of env
pub async fn query_influx() -> Vec<InfluxReadableGpsPoint> {
    info!("Open token file, using env var TOKEN_FILE");
    let token_file = match std::env::var("TOKEN_FILE") {
        Ok(value) => value,
        Err(_) => panic!("Failed to get token file exiting"),
    };

    info!("Read token file to string");
    let mut file = File::open(token_file).unwrap();
    let mut token: String = "".to_string();
    file.read_to_string(&mut token).unwrap();
    drop(file);

    info!("Open start date file, using env var START_DATE_FILE");
    let token_file = match std::env::var("START_DATE_FILE") {
        Ok(value) => value,
        Err(_) => panic!("Failed to get start date file exiting"),
    };

    info!("Read start date file to string");
    let mut file = File::open(token_file).unwrap();
    let mut start_date: String = "".to_string();
    file.read_to_string(&mut start_date).unwrap();
    drop(file);

    info!("Open end date file, using env var END_DATE_FILE");
    let token_file = match std::env::var("END_DATE_FILE") {
        Ok(value) => value,
        Err(_) => panic!("Failed to get token file exiting"),
    };

    info!("Read end date file to string");
    let mut file = File::open(token_file).unwrap();
    let mut end_date: String = "".to_string();
    file.read_to_string(&mut end_date).unwrap();
    drop(file);

    let host = env!("INFLUXDB_HOST");
    let org = env!("INFLUXDB_ORG");
    info!("connecting to influx using host: {}, and org {}", host, org);

    let client = Arc::new(Mutex::new(Client::new(host, org, &token)));

    let start_time_nano = start_date
        .parse::<DateTime<FixedOffset>>()
        .unwrap()
        .timestamp();

    let end_time_nano = end_date
        .parse::<DateTime<FixedOffset>>()
        .unwrap()
        .timestamp();

    let qs = format!(
        "from(bucket: \"{}\")
        |> range(start: {}, stop: {}) |> filter(fn: (r) => r[\"_measurement\"] == \"rawgps\")",
        env!("INFLUXDB_BUCKET"),
        start_time_nano,
        end_time_nano
    );
    debug!("Running query: {qs}");
    let query = Query::new(qs.to_string());
    // let query = Query::new(r#"from(bucket: "personal")|> range(start: -200d, stop: -199d)|> filter(fn: (r) => r["_measurement"] == "rawgps")"#.to_owned());
    let x = client
        .lock()
        .await
        .query::<InfluxReadableGpsPoint>(Some(query))
        .await
        .unwrap();
    x
}

pub fn locally_cache_influx_data(res: Vec<InfluxReadableGpsPoint>) -> Result<(), CacheError> {
    // Serialize it to a JSON string.
    let j = serde_json::to_string(&res).or(Err("Failed to serialise vec".to_string()))?;

    // Print, write to a file, or send to an HTTP server.
    // println!("{}", j);
    let mut file = File::create("temptiny718m.json").or(Err("Failed to create file".to_owned()))?;
    file.write_all(j.as_bytes())
        .or(Err("Failed to write serialised struct to file".to_owned()))?;

    Ok(())
}

#[allow(dead_code)]
fn log_influx(client: Arc<Mutex<Client>>, points: Vec<InfluxWritableGpsPoint>) -> JoinHandle<()> {
    tokio::spawn(async move {
        match client
            .lock()
            .await
            .write(env!("INFLUXDB_BUCKET"), stream::iter(points))
            .await
        {
            Ok(_) => {}
            Err(err) => {
                println!("ddasd");
                println!("{:?}", err);
                // error!("failed to send to influx, {:?}", err);
            }
        }
    })
}

pub fn distance_between_influx_points(
    point1: &InfluxReadableGpsPoint,
    point2: &InfluxReadableGpsPoint,
) -> f64 {
    let alt_diff = (point1.ele - point2.ele).abs();
    let geo_point1: geo::Point<f64> = point1.into();
    let geo_point2: Point<f64> = point2.into();
    let distance = geo_point1.geodesic_distance(&geo_point2);
    (distance.powi(2) + alt_diff.powi(2)).sqrt()
}

type CacheError = String;
