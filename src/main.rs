use clap::{Args, Parser, Subcommand};
use egui::{Key, ScrollArea};
use log::{trace, LevelFilter};
use rawgpx_to_influx::influx::{locally_cache_influx_data, query_influx, InfluxReadableGpsPoint};
use rawgpx_to_influx::methods::from_cache::{from_cache, FromCache};
use std::path::PathBuf;
// use crossterm::command::ExecutableCommand;
// use std::{
//     io::{self, stdout, Stdout},
//     time::{Duration, Instant},
// };
//
// use crossterm::{
//     event::{self, Event, KeyCode},
//     terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
//     ExecutableCommand,
// };
// use ratatui::{
//     prelude::*,
//     widgets::{canvas::*, *},
// };

/// todo pull down some data from 06:07:00 UTC to 7:52 this is my walk to work so 12km
/// after that quite a but of time shouldnt add any more walking on like 12:51 still didnt leave, i started walking sometime around 2 so pull down
/// some data till 7:52 to tune walking then till 12 to tune being still then after 2 to add back the walking

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Cli {
    #[arg(long, short = 'v', action = clap::ArgAction::Count, global = true,)]
    verbose: u8,

    /// Main action to do
    #[command(subcommand)]
    action: Actions,
}

#[derive(Subcommand, Debug, Clone)]
enum Actions {
    Cache(Cache),
    FromInflux(FromInflux),
    FromCache(FromCache),
    Gui,
    Tui,
    // gpxtoinflux
}

#[derive(Parser, Debug, Clone)]
struct Cache {
    #[command(flatten)]
    influx_args: InfluxArguments,

    #[arg(long)]
    cache_file: PathBuf,
}

#[derive(Parser, Debug, Clone)]
struct FromInflux {
    #[command(flatten)]
    influx_args: InfluxArguments,
}

#[derive(Args, Debug, Clone)]
struct InfluxArguments {
    // INFLUXDB_HOST
    // INFLUXDB_ORG
    // INFLUXDB_BUCKET
    // INFLUXDB_MEASUREMENT
    // TOKEN_FILE
    // START date
    // end date
}

#[tokio::main]
async fn main() {
    let args = Cli::parse();
    match args.verbose {
        0 => {
            env_logger::init();
        }
        1 => {
            env_logger::Builder::new()
                .filter_level(LevelFilter::Warn)
                .init();
        }
        2 => {
            env_logger::Builder::new()
                .filter_level(LevelFilter::Info)
                .init();
        }
        3 => {
            env_logger::Builder::new()
                .filter_level(LevelFilter::Debug)
                .init();
        }
        4 => {
            env_logger::Builder::new()
                .filter_level(LevelFilter::Trace)
                .init();
        }
        5_u8..=u8::MAX => {
            env_logger::Builder::new()
                .filter_level(LevelFilter::Trace)
                .init();
        }
    }

    match args.action {
        Actions::FromCache(sub_args) => {
            from_cache(sub_args).await;
        }
        Actions::FromInflux(_sub_args) => {
            let _res = query_influx().await;
            // do stuff
        }
        Actions::Cache(_sub_args) => {
            let res = query_influx().await;
            locally_cache_influx_data(res).unwrap();
        }
        Actions::Gui => {
            let _ = dbg!(launch_egui());
        }
        Actions::Tui => {
            let _ = dbg!(App::run());
        }
    }
}

fn launch_egui() -> Result<(), eframe::Error> {
    let options = eframe::NativeOptions::default();
    eframe::run_native(
        "Keyboard events",
        options,
        Box::new(|_cc| Box::<Content>::default()),
    )
}

#[derive(Default)]
struct Content {
    text: String,
}

impl eframe::App for Content {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.heading("Press/Hold/Release example. Press A to test.");
            if ui.button("Clear").clicked() {
                self.text.clear();
            }
            ScrollArea::vertical()
                .auto_shrink(false)
                .stick_to_bottom(true)
                .show(ui, |ui| {
                    ui.label(&self.text);
                });

            if ctx.input(|i| i.key_pressed(Key::A)) {
                self.text.push_str("\nPressed");
            }
            if ctx.input(|i| i.key_down(Key::A)) {
                self.text.push_str("\nHeld");
                ui.ctx().request_repaint(); // make sure we note the holding.
            }
            if ctx.input(|i| i.key_released(Key::A)) {
                self.text.push_str("\nReleased");
            }
        });
    }
}

// fn main() -> io::Result<()> {
//     App::run()
// }

use std::fs::File;
use std::io::Read;
use std::{
    io::{self, stdout, Stdout},
    time::{Duration, Instant},
};

use crossterm::event::MouseEventKind;
use crossterm::{
    event::{self, Event, KeyCode},
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
    ExecutableCommand,
};
use ratatui::{
    prelude::*,
    widgets::{canvas::*, *},
};
use rawgpx_to_influx::point_structs::PointWithMetadata;

struct App {
    x: f64,
    y: f64,
    viewport: Viewport,
    points: Vec<PointWithMetadata>,
    selected_point: Option<usize>,
    tick_count: u64,
    marker: Marker,
    mouse_down_capture: MouseDownCapture,
    mouse_clicked: Option<MouseClicked>,
}

#[derive(Debug)]
struct MouseClicked {
    col: u16,
    row: u16,
}

struct MouseDownCapture {
    instant: Instant,
    col: u16,
    row: u16,
}

struct MouseUpCapture {
    _instant: Instant,
    col: u16,
    row: u16,
}

struct Viewport {
    view: View,
    /// middle point on screen
    midpoint: Midpoint,
}

impl Viewport {
    fn lat_bounds(&self) -> [f64; 2] {
        let left_bound = self.midpoint.lat - (self.view.lat / 2.0);
        let right_bound = self.midpoint.lat + (self.view.lat / 2.0);
        [left_bound, right_bound]
    }

    fn lon_bounds(&self) -> [f64; 2] {
        let bottom_bound = self.midpoint.lon - (self.view.lon / 2.0);
        let top_bound = self.midpoint.lon + (self.view.lon / 2.0);
        [bottom_bound, top_bound]
    }

    fn zoom(&mut self, times: f64) {
        self.view.lat *= times;
        self.view.lon *= times;
    }

    fn window_string(&self) -> String {
        format!(
            "scale ({}:{}), midpoint ({}:{})",
            self.view.lat, self.view.lon, self.midpoint.lat, self.midpoint.lon
        )
    }

    /// move left by 10%
    fn move_left(&mut self) {
        self.midpoint.lon = self.midpoint.lon - (self.view.lon / 10.0);
    }

    /// move left by 10%
    fn move_right(&mut self) {
        self.midpoint.lon = self.midpoint.lon + (self.view.lon / 10.0);
    }

    /// move left by 10%
    fn move_down(&mut self) {
        self.midpoint.lat = self.midpoint.lat - (self.view.lat / 10.0);
    }

    /// move left by 10%
    fn move_up(&mut self) {
        self.midpoint.lat = self.midpoint.lat + (self.view.lat / 10.0);
    }
}

/// number of degrees visible on the screen
struct View {
    lat: f64,
    lon: f64,
}

struct Midpoint {
    lat: f64,
    lon: f64,
}

fn confirm_click(
    prev_mouse_down_caupture: &MouseDownCapture,
    mouse_up_capture: MouseUpCapture,
) -> Option<MouseClicked> {
    if prev_mouse_down_caupture.col == mouse_up_capture.col
        && prev_mouse_down_caupture.row == mouse_up_capture.row
    {
        Some(MouseClicked {
            col: prev_mouse_down_caupture.col,
            row: prev_mouse_down_caupture.row,
        })
    } else {
        None
    }
}

impl App {
    fn new() -> Self {
        Self {
            x: 0.0,
            y: 0.0,

            viewport: Viewport {
                view: View {
                    lat: 180.0,
                    lon: 360.0,
                },
                midpoint: Midpoint { lat: 0.0, lon: 0.0 },
            },
            points: Vec::new(),
            selected_point: None,
            tick_count: 0,
            marker: Marker::HalfBlock,

            mouse_down_capture: MouseDownCapture {
                instant: Instant::now(),
                col: 0,
                row: 0,
            },
            mouse_clicked: None,
        }
    }

    pub fn run() -> io::Result<()> {
        let mut terminal = init_terminal()?;
        let mut app = Self::new();
        let mut last_tick = Instant::now();
        let tick_rate = Duration::from_millis(16);

        let mut file = File::open("temp.json").unwrap();
        let mut string_file: String = "".to_string();
        file.read_to_string(&mut string_file).unwrap();
        drop(file);
        let p: Vec<InfluxReadableGpsPoint> = serde_json::from_str(&string_file).unwrap();
        drop(string_file);
        let mut metadata: Vec<PointWithMetadata> = p.iter().map(|x| x.into()).collect();
        metadata.sort_by(|point1, point2| point1.time.cmp(&point2.time));
        app.points = metadata;

        loop {
            let _ = terminal.draw(|frame| app.ui(frame));
            let timeout = tick_rate.saturating_sub(last_tick.elapsed());
            if event::poll(timeout)? {
                match event::read() {
                    Ok(Event::Key(key)) => match key.code {
                        KeyCode::Char('q') => break,
                        KeyCode::Down | KeyCode::Char('j') => app.viewport.move_down(),
                        KeyCode::Up | KeyCode::Char('k') => app.viewport.move_up(),
                        KeyCode::Right | KeyCode::Char('l') => app.viewport.move_right(),
                        KeyCode::Left | KeyCode::Char('h') => app.viewport.move_left(),
                        KeyCode::Char('+') | KeyCode::Char('=') => app.viewport.zoom(0.5),
                        KeyCode::Char('-') | KeyCode::Char('_') => app.viewport.zoom(2.0),
                        _ => {}
                    },
                    Ok(Event::Mouse(mouse_event)) => match mouse_event.kind {
                        MouseEventKind::Down(_) => {
                            app.mouse_down_capture.instant = Instant::now();
                            app.mouse_down_capture.col = mouse_event.column;
                            app.mouse_down_capture.row = mouse_event.row;
                        }
                        MouseEventKind::Up(_) => {
                            app.mouse_clicked = confirm_click(
                                &app.mouse_down_capture,
                                MouseUpCapture {
                                    _instant: Instant::now(),
                                    col: mouse_event.column,
                                    row: mouse_event.row,
                                },
                            );
                        }
                        MouseEventKind::Drag(_) => {}
                        MouseEventKind::Moved => {}
                        MouseEventKind::ScrollDown => app.viewport.zoom(2.0),
                        MouseEventKind::ScrollUp => app.viewport.zoom(0.5),
                        MouseEventKind::ScrollLeft => {}
                        MouseEventKind::ScrollRight => {}
                    },
                    Ok(_) => {}
                    Err(_) => {}
                }
            }

            if last_tick.elapsed() >= tick_rate {
                app.on_tick();
                last_tick = Instant::now();
            }
        }
        restore_terminal()
    }

    fn on_tick(&mut self) {
        self.tick_count += 1;
        // only change marker every 180 ticks (3s) to avoid stroboscopic effect
        // if (self.tick_count % 180) == 0 {
        //     self.marker = match self.marker {
        //         Marker::Dot => Marker::Braille,
        //         Marker::Braille => Marker::Block,
        //         Marker::Block => Marker::HalfBlock,
        //         Marker::HalfBlock => Marker::Bar,
        //         Marker::Bar => Marker::Dot,
        //     };
        // }
        // bounce the ball by flipping the velocity vector
        // let ball = &self.ball;
        // let playground = self.playground;
        // if ball.x - ball.radius < f64::from(playground.left())
        //     || ball.x + ball.radius > f64::from(playground.right())
        // {
        //     self.vx = -self.vx;
        // }
        // if ball.y - ball.radius < f64::from(playground.top())
        //     || ball.y + ball.radius > f64::from(playground.bottom())
        // {
        //     self.vy = -self.vy;
        // }

        // self.ball.x += self.vx;
        // self.ball.y += self.vy;
        //
    }

    fn ui(&mut self, frame: &mut Frame) {
        let horizontal =
            Layout::horizontal([Constraint::Percentage(90), Constraint::Percentage(10)]);
        // let vertical = Layout::vertical([Constraint::Percentage(50), Constraint::Percentage(50)]);
        let [map, _right] = horizontal.areas(frame.size());
        // dbg!(map.x);
        // map.height
        if let Some(mouse_clicked) = &self.mouse_clicked {
            let mouse_coord = col_row_to_coord(
                mouse_clicked.col,
                mouse_clicked.row,
                &self.viewport,
                map.height,
                map.width,
            );
            self.selected_point = closest_point_to_coord(
                mouse_coord,
                &self.points,
                &self.viewport,
                map.height,
                map.width,
            );
        }

        frame.render_widget(self.map_canvas(map.width, map.height), map);
    }

    fn map_canvas(&self, width: u16, height: u16) -> impl Widget + '_ {
        // let mut mouse_coord = (0.0, 0.0);
        // if let Some(mouse_clicked) = &self.mouse_clicked {
        //     mouse_coord =  col_row_to_coord(mouse_clicked.col, mouse_clicked.row, &self.viewport, height, width);
        // }

        Canvas::default()
            .block(
                Block::default()
                    .borders(Borders::ALL)
                    .title(match &self.mouse_clicked {
                        None => {
                            format!(
                                "{}:::: {},{}",
                                self.viewport.window_string(),
                                self.mouse_down_capture.col,
                                self.mouse_down_capture.row
                            )
                        }
                        Some(m_click) => {
                            format!(
                                // "{} :::: {}:{}, {:?}",
                                "{} :::: {}:{}",
                                self.viewport.window_string(),
                                m_click.row,
                                m_click.col,
                                // mouse_coord
                            )
                        }
                    }),
            )
            .marker(self.marker)
            .paint(move |ctx| {
                ctx.draw(&Map {
                    color: Color::Green,
                    resolution: MapResolution::High,
                });
                ctx.draw(&canvas::Line::new(5.0, 5.0, 10.0, 10.0, Color::Red));
                ctx.print(self.x, -self.y, "You are here".yellow());
                display_points(
                    &self.points,
                    &self.viewport,
                    height,
                    width,
                    ctx,
                    &self.selected_point,
                );

                // ctx
            })
            .x_bounds(self.viewport.lon_bounds())
            .y_bounds(self.viewport.lat_bounds())
    }
}

fn closest_point_to_coord(
    coords: (f64, f64),
    points: &Vec<PointWithMetadata>,
    viewport: &Viewport,
    height: u16,
    width: u16,
) -> Option<usize> {
    let mouse_click_point = PointWithMetadata {
        time: Default::default(),
        lat: coords.1,
        lon: coords.0,
        ele: None,
        s2_cell_id: None,
        recording_app: None,
        recording_device: None,
        metadata: Default::default(),
    };
    let mut max_prev_pixel_diff = 5.0;
    let mut close_mouse_point: Option<usize> = None;

    let lon_deg_per_pix = viewport.view.lon / <u16 as TryInto<f64>>::try_into(height).unwrap(); //x
    let lat_deg_per_pix = viewport.view.lat / <u16 as TryInto<f64>>::try_into(width).unwrap(); //y

    let mut display_point: PointWithMetadata = points[0].clone();
    // todo move out to function
    {
        let abs_x_pixel_diff =
            (mouse_click_point.lon - display_point.lon).abs() / lon_deg_per_pix.abs();
        let abs_y_pixel_diff =
            (mouse_click_point.lat - display_point.lat).abs() / lat_deg_per_pix.abs();
        let dist_pix = (abs_x_pixel_diff.powi(2) + abs_y_pixel_diff.powi(2)).sqrt();
        if dist_pix < max_prev_pixel_diff {
            close_mouse_point = Some(0);
            max_prev_pixel_diff = dist_pix;
        }
    }

    trace!("iterate through display points and compare distances");
    for (index, point) in points.iter().enumerate().skip(1) {
        if (point.lon - display_point.lon).abs() > lon_deg_per_pix {
            display_point = point.clone();

            // todo move out to function
            {
                let abs_x_pixel_diff =
                    (mouse_click_point.lon - display_point.lon).abs() / lon_deg_per_pix.abs();
                let abs_y_pixel_diff =
                    (mouse_click_point.lat - display_point.lat).abs() / lat_deg_per_pix.abs();
                let dist_pix = (abs_x_pixel_diff.powi(2) + abs_y_pixel_diff.powi(2)).sqrt();
                if dist_pix < max_prev_pixel_diff {
                    close_mouse_point = Some(index);
                    max_prev_pixel_diff = dist_pix;
                }
            }
        } else if (point.lat - display_point.lat).abs() > lat_deg_per_pix {
            display_point = point.clone();
            // todo move out to function
            {
                let abs_x_pixel_diff =
                    (mouse_click_point.lon - display_point.lon).abs() / lon_deg_per_pix.abs();
                let abs_y_pixel_diff =
                    (mouse_click_point.lat - display_point.lat).abs() / lat_deg_per_pix.abs();
                let dist_pix = (abs_x_pixel_diff.powi(2) + abs_y_pixel_diff.powi(2)).sqrt();
                if dist_pix < max_prev_pixel_diff {
                    close_mouse_point = Some(index);
                    max_prev_pixel_diff = dist_pix;
                }
            }
        }
    }

    close_mouse_point
}

/// we go from top left and we are from the whole terminal not the panel
/// also the border is 1 px
/// atm i dont see any way to get the boder thinkness
fn col_row_to_coord(
    col: u16,
    row: u16,
    viewport: &Viewport,
    height: u16,
    width: u16,
) -> (f64, f64) {
    let lon_deg_per_pix: f64 = viewport.view.lon / <u16 as TryInto<f64>>::try_into(width).unwrap(); //x
    let lat_deg_per_pix: f64 = viewport.view.lat / <u16 as TryInto<f64>>::try_into(height).unwrap(); //y

    let top_left_coord: (f64, f64) = (
        (viewport.midpoint.lon - (viewport.view.lon / 2.0)),
        (viewport.midpoint.lat + (viewport.view.lat / 2.0)),
    );

    if col > width - 2 {
        return (0.0, 0.0);
    }

    // zero row/col is the border
    let lon_click = top_left_coord.0
        + (lon_deg_per_pix * (<u16 as TryInto<f64>>::try_into(col).unwrap() - 1.0));
    let lat_click = top_left_coord.1
        - (lat_deg_per_pix * (<u16 as TryInto<f64>>::try_into(row).unwrap() - 1.0));

    (lon_click, lat_click)
}

fn display_points(
    points: &Vec<PointWithMetadata>,
    viewport: &Viewport,
    height: u16,
    width: u16,
    context: &mut Context,
    selected_point: &Option<usize>,
) {
    let lon_deg_per_pix = viewport.view.lon / <u16 as TryInto<f64>>::try_into(height).unwrap(); //x
    let lat_deg_per_pix = viewport.view.lat / <u16 as TryInto<f64>>::try_into(width).unwrap(); //y

    let mut orig_point: PointWithMetadata = points[0].clone();
    for (index, point) in points.iter().enumerate().skip(1) {
        let colour = match selected_point {
            None => Color::Blue,
            Some(selected_index) => {
                if *selected_index == index {
                    Color::Red
                } else {
                    Color::Blue
                }
            }
        };
        if (point.lon - orig_point.lon).abs() > lon_deg_per_pix {
            context.draw(&canvas::Line::new(
                orig_point.lon,
                orig_point.lat,
                point.lon,
                point.lat,
                colour,
            ));
            orig_point = point.clone();
        } else if (point.lat - orig_point.lat).abs() > lat_deg_per_pix {
            context.draw(&canvas::Line::new(
                orig_point.lon,
                orig_point.lat,
                point.lon,
                point.lat,
                colour,
            ));
            orig_point = point.clone();
        }
    }

    if orig_point.lat == points[0].lat && orig_point.lon == points[0].lon {
        context.draw(&canvas::Line::new(
            orig_point.lon,
            orig_point.lat,
            orig_point.lon,
            orig_point.lat,
            Color::Blue,
        ));
    }
}

#[allow(dead_code)]
struct Line {
    x1: f64,
    y1: f64,
    x2: f64,
    y2: f64,
}

/*fn transform_line(line: Line, canvas_width: f64, canvas_height: f64, viewport: Viewport) -> Line {
    let x_length = line.x2 - line.x1;
    let y_length = line.y2 - line.y1;

    let x_pixels = (canvas_width / viewport.view.lat) * x_length;
    let y_pixels = (canvas_height / viewport.view.lon) * y_length;

    let x_start_pos = line.x1
}*/

fn init_terminal() -> io::Result<Terminal<CrosstermBackend<Stdout>>> {
    enable_raw_mode()?;
    stdout()
        .execute(EnterAlternateScreen)?
        .execute(crossterm::event::EnableMouseCapture)?;
    Terminal::new(CrosstermBackend::new(stdout()))
}

fn restore_terminal() -> io::Result<()> {
    disable_raw_mode()?;
    stdout().execute(LeaveAlternateScreen)?;
    Ok(())
}
