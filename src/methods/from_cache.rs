use crate::influx::distance_between_influx_points;
use crate::point_helpers::distance_between_metadata_points;
use crate::point_structs::PhysicalActivity::Walk;
use crate::point_structs::RuleInstantaneous::RuleInstantaneousSpeed;
use crate::point_structs::RuleSlidingWindow::Speed;
use crate::point_structs::{
    Activity, ExpectedValue, InsuficentDataAction, InterpolateGPSPoints, PointWithMetadata, Rule,
    RuleSet, RuleSlidingWindowSpeed, RuleWeight, SlidingWindow,
};
use crate::rule_processors::process_rules;
use crate::window_helpers::process_sliding_windows_on_data;
use crate::InfluxReadableGpsPoint;
use clap::Parser;
use log::trace;
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use uom::fmt::DisplayStyle::Abbreviation;
use uom::si::f64::*;
use uom::si::length::kilometer;
use uom::si::length::meter;
use uom::si::velocity::kilometer_per_hour;

#[derive(Parser, Debug, Clone)]
pub struct FromCache {
    #[arg(long)]
    pub cache_file: PathBuf,
}

pub async fn from_cache(args: FromCache) {
    let mut file = File::open(args.cache_file).unwrap();
    let mut string_file: String = "".to_string();
    file.read_to_string(&mut string_file).unwrap();
    drop(file);
    let p: Vec<InfluxReadableGpsPoint> = serde_json::from_str(&string_file).unwrap();
    drop(string_file);

    // println!("{:?}", p);

    let distance: f64 = p.windows(2).fold(0.0, |val, points| {
        val + distance_between_influx_points(&points[0], &points[1])
    });

    let mut metadata: Vec<PointWithMetadata> = p.iter().map(|x| x.into()).collect();
    /*let metadata = */
    metadata.sort_by(|point1, point2| point1.time.cmp(&point2.time));
    let mut rule_sets: HashMap<Activity, RuleSet> = HashMap::new();
    rule_sets.insert(
        Activity::PhysicalActivity(Walk),
        RuleSet {
            rules_with_weights: vec![
                RuleWeight {
                    rule: Rule::Instantaneous(RuleInstantaneousSpeed(ExpectedValue::LessThan(
                        Velocity::new::<kilometer_per_hour>(8.0),
                    ))),
                    weight: 1.0,
                    negative_weight: 0.0,
                    insuficent_data_action: InsuficentDataAction::Ignore,
                },
                RuleWeight {
                    rule: Rule::Instantaneous(RuleInstantaneousSpeed(ExpectedValue::GreaterThan(
                        Velocity::new::<kilometer_per_hour>(1.0),
                    ))),
                    weight: 1.0,
                    negative_weight: 0.0,
                    insuficent_data_action: InsuficentDataAction::Ignore,
                },
                RuleWeight {
                    rule: Rule::SlidingWindow(Speed(RuleSlidingWindowSpeed {
                        window: SlidingWindow {
                            period: chrono::Duration::seconds(300),
                            interpolate: InterpolateGPSPoints::No(chrono::Duration::seconds(5)),
                            offset: chrono::Duration::seconds(-150),
                        },
                        expected_value: ExpectedValue::LessThan(
                            Velocity::new::<kilometer_per_hour>(8.0),
                        ),
                    })),
                    weight: 1.0,
                    negative_weight: 0.0,
                    insuficent_data_action: InsuficentDataAction::Ignore,
                },
                RuleWeight {
                    rule: Rule::SlidingWindow(Speed(RuleSlidingWindowSpeed {
                        window: SlidingWindow {
                            period: chrono::Duration::seconds(300),
                            interpolate: InterpolateGPSPoints::No(chrono::Duration::seconds(5)),
                            offset: chrono::Duration::seconds(-150),
                        },
                        expected_value: ExpectedValue::GreaterThan(Velocity::new::<
                            kilometer_per_hour,
                        >(1.0)),
                    })),
                    weight: 1.0,
                    negative_weight: 0.0,
                    insuficent_data_action: InsuficentDataAction::Ignore,
                },
            ],
            activity: Activity::PhysicalActivity(Walk),
            activation_confidence: 2.0,
        },
    );
    /*let rule_sets = vec![RuleSet {
        rules_with_weights: vec![
            RuleWeight {
                rule: Rule {
                    speed_window: RuleSpeedWindow::Instantaneous,
                    expected_value: ExpectedValue::LessThan(8.0),
                },
                weight: 1.0,
                negative_weight: 0.0,
                insuficent_data_action: InsuficentDataAction::DelaireRuleFailed,
            },
            RuleWeight {
                rule: Rule {
                    speed_window: RuleSpeedWindow::Instantaneous,
                    expected_value: ExpectedValue::LessThan(1.0),
                },
                weight: 1.0,
                negative_weight: 0.0,
                insuficent_data_action: InsuficentDataAction::DelaireRuleFailed,
            },
            RuleWeight {
                rule: Rule {
                    speed_window: RuleSpeedWindow::SlidingWindow(SlidingWindow {
                        period: chrono::Duration::seconds(300), // time::Duration::new(300, 0),
                        interpolate: InterpolateGPSPoints::No(chrono::Duration::seconds(5)), //time::Duration::new(5, 0),
                        offset: chrono::Duration::seconds(150), //time::Duration::new(150, 0),
                        derivative: Derivative::Speed,
                    }),
                    expected_value: ExpectedValue::LessThan(8.0),
                },
                weight: 1.0,
                negative_weight: 0.0,
                insuficent_data_action: InsuficentDataAction::DelaireRuleFailed,
            },
            RuleWeight {
                rule: Rule {
                    speed_window: RuleSpeedWindow::SlidingWindow(SlidingWindow {
                        period: chrono::Duration::seconds(300), // time::Duration::new(300, 0),
                        interpolate: InterpolateGPSPoints::No(chrono::Duration::seconds(5)), //time::Duration::new(5, 0),
                        offset: chrono::Duration::seconds(150), //time::Duration::new(150, 0),
                        derivative: Derivative::Speed,
                    }),
                    expected_value: ExpectedValue::GreaterThan(1.0),
                },
                weight: 1.0,
                negative_weight: 0.0,
                insuficent_data_action: InsuficentDataAction::DelaireRuleFailed,
            },
        ],
        activity: Activity::PhysicalActivity(Walk),
        activation_confidence: 2.0,
    }];*/

    let processed_points = process_sliding_windows_on_data(metadata, &rule_sets);
    let processed_points = process_rules(processed_points, rule_sets);

    let walking_distacnce = processed_points
        .all_points
        .windows(2)
        .fold(
            Length::new::<meter>(5.0), |distance, points|
                {
                    trace!("time: {:?}, activity: {:?}, all_activities: {:?}, speeds: {:?}, instanatanious {:?}", points[1].time, points[1].metadata.predicted_activity, points[1].metadata.activitys, points[1].metadata.sliding_speed_window_values, points[1].metadata.instantaneous_speed);
                    if points[1].metadata.predicted_activity == Activity::PhysicalActivity(Walk)
                    {
                        distance + distance_between_metadata_points(&points[0], &points[1])
                    } else {
                        distance
                    }
                }
        );

    println!("distance is {} km", distance);
    println!(
        "walking distance is {}",
        walking_distacnce.into_format_args(kilometer, Abbreviation)
    );
}
