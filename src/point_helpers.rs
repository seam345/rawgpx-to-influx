use crate::point_structs::PointWithMetadata;
use chrono::{DateTime, FixedOffset};
use geo::{GeodesicDistance, Point};
use log::{debug, trace};
use num_traits::ToPrimitive;
use uom::si::f64::*;
use uom::si::length::meter;
use uom::si::time::second;
use uom::si::velocity::kilometer_per_hour;

// /// Plan is to take 1 (maybe at some point many) gpx files and add as much data we can compute to it
// /// include predictions on means of travel
// fn add_computable_metadata(gpx: Gpx) -> Vec<PointWithMetadata> {
//     unimplemented!()
// }
//
// /// Using computed data, convert into debug gpx file
// fn generate_gpx(gpx: Vec<PointWithMetadata>) -> Gpx {
//     unimplemented!()
// }

/// Using computed data, convert into debug influxDB import
#[allow(dead_code)]
fn generate_influx(gpx: Vec<PointWithMetadata>) {
    let _to_use = gpx;
    // todo decide if this returns something (say csv text) or connects to influx and writes directly
    unimplemented!()
}

/// Using computed data try to clean up the file of incorrect gps points
#[allow(dead_code)]
fn remove_bad_gps_points(gpx: Vec<PointWithMetadata>) -> Vec<PointWithMetadata> {
    let _to_use = gpx;
    unimplemented!()
}

/// Using computed data, snap to predicted location
#[allow(dead_code)]
fn improve_gps_track(gpx: Vec<PointWithMetadata>) -> Vec<PointWithMetadata> {
    let _to_use = gpx;
    unimplemented!()
}

/// Using computed data, shrink file by removing unessasery points, like multiple points inside building
#[allow(dead_code)]
fn shrink_gps_track(gpx: Vec<PointWithMetadata>) -> Vec<PointWithMetadata> {
    let _to_use = gpx;
    unimplemented!()
}

/// Calculates distance between 2 points (using pythagorious to calculate height change distance)
/// returns distance in meters
///
pub fn distance_between_points(
    point1: geo::Point,
    ele1: f64,
    point2: geo::Point,
    ele2: f64,
) -> f64 {
    let distance = point1.geodesic_distance(&point2);
    let alt_diff = (ele1 - ele2).abs();
    (distance.powi(2) + alt_diff.powi(2)).sqrt()
}

pub fn distance_between_metadata_points(
    point1: &PointWithMetadata,
    point2: &PointWithMetadata,
) -> Length {
    let geo_point1: geo::Point<f64> = point1.into();
    let geo_point2: Point<f64> = point2.into();
    let distance = Length::new::<meter>(geo_point1.geodesic_distance(&geo_point2));
    match (&point1.ele, &point2.ele) {
        (Some(p1_ele), Some(p2_ele)) => {
            let alt_diff: Length = Length::new::<meter>((p1_ele - p2_ele).abs());
            let distance_with_ele = (distance * distance + alt_diff * alt_diff).sqrt();
            distance_with_ele
        }
        (None, None) | (Some(_), None) | (None, Some(_)) => distance,
    }
}

pub fn speed(waypoint1: &PointWithMetadata, waypoint2: &PointWithMetadata) -> Velocity {
    let maybe: DateTime<FixedOffset> = waypoint1.time;
    let maybe2: DateTime<FixedOffset> = waypoint2.time;

    let erm = maybe - maybe2;
    if erm == chrono::Duration::seconds(0) {
        debug!("duration was 0");
        Velocity::new::<kilometer_per_hour>(0.0)
    } else {
        let distance = distance_between_metadata_points(waypoint1, waypoint2);
        trace!("");
        let speed_meters_per_second = distance
            / Time::new::<second>(
                erm.num_seconds()
                    .abs()
                    .to_f64()
                    .expect("Failed to convert i64 to f64"),
            );
        speed_meters_per_second
    }
}

#[allow(dead_code)]
fn acceleration(
    waypoint1: &PointWithMetadata,
    waypoint2: &PointWithMetadata,
    waypoint3: &PointWithMetadata,
) -> Acceleration {
    let speed_difference = speed(waypoint2, waypoint3) - speed(waypoint1, waypoint2);
    // above I'm assuming I calculated the speed I'm going at point 2 and point 3 and point 1 is just unknowable
    // from this I can now wor out how quickly I accelerated from point 2 to point 3
    // let maybe2 = OffsetDateTime::parse(&waypoint2.time.unwrap().format().expect("hey"), &Iso8601::DEFAULT).unwrap();
    let maybe2 = waypoint2.time;
    // let maybe3 = OffsetDateTime::parse(&waypoint3.time.unwrap().format().expect("hey"), &Iso8601::DEFAULT).unwrap();
    let maybe3 = waypoint3.time;
    let time_difference = maybe3 - maybe2;

    let speed_meters_per_second_per_second = speed_difference
        / Time::new::<second>(
            time_difference
                .num_seconds()
                .abs()
                .to_f64()
                .expect("Failed to convert i64 to f64"),
        );

    speed_meters_per_second_per_second
}

/*

/// calculates if point should be considered walking
/// next_5_min 1 f64 for every minute, the further into the array we go the more ahead of time we are
/// prev_5_min 1 f64 for every minute, the further into the array we go the more behind of time we are
fn is_on_foot(speed: f64, next_5_minute_avg_speed: &[Option<f64>], previous_5_minute_avg_speed: &[Option<f64>], next_5_2minute_avg_speed: &[Option<f64>], previous_5_2minute_avg_speed: &[Option<f64>]) -> bool {
    match next_5_minute_avg_speed[0] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[0] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }

    match next_5_minute_avg_speed[1] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[1] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }

    match next_5_minute_avg_speed[0] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[1] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
    match next_5_minute_avg_speed[0] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[2] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }

    match next_5_minute_avg_speed[1] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[2] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
    match next_5_minute_avg_speed[1] {
        Some(next_min_speed) => {
            match previous_5_minute_avg_speed[0] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }

    match next_5_2minute_avg_speed[0] {
        Some(next_min_speed) => {
            match previous_5_2minute_avg_speed[0] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
    match next_5_2minute_avg_speed[1] {
        Some(next_min_speed) => {
            match previous_5_2minute_avg_speed[0] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
    match next_5_2minute_avg_speed[2] {
        Some(next_min_speed) => {
            match previous_5_2minute_avg_speed[0] {
                Some(prev_min_speed) => {
                    if prev_min_speed < TOP_ON_FOOT_SPEED && next_min_speed < TOP_ON_FOOT_SPEED {
                        return true;
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }


    if speed < TOP_ON_FOOT_SPEED {
        return true;
    }

    false
}

fn get_previous_x_minutes_avg_speed(x_min: usize, points: &Vec<Waypoint>, current_point_pos: usize) -> Vec<Option<f64>> {
    let current_points_time: OffsetDateTime = points[current_point_pos].time.unwrap().into();
    // println!("{:?}", current_points_time);
    let mut array_vec: Vec<Vec<f64>> = Vec::new();
    for i in 0..x_min {
        array_vec.push(Vec::new());
    }


    for point in points.windows(2).take(current_point_pos).rev() {
        let prev_point_time: OffsetDateTime = point[0].time.unwrap().into();
        // println!("{:?}", prev_point_time);
        let min_diff = ((current_points_time - prev_point_time).as_seconds_f64() / 60.0).floor() as usize;
        if min_diff >= x_min {
            break;
        }
        // let index = min_diff.floor() as usize;
        let speed = speed(&point[0], &point[1]);
        array_vec[min_diff].push(speed);


        // println!("{:?}", point);
        // println!("{:?}", min_diff);
    }
    // println!("{:#?}", array_vec);
    let mut result_vec: Vec<Option<f64>> = Vec::new();
    for i in array_vec.into_iter() {
        if i.len() == 0 {
            result_vec.push(None);
        } else {
            result_vec.push(Some(average(&i[0..i.len()])));
        }
        // println!("MODE: {}", mode(&i));
        // println!("MEDIAN: {}", median(&mut i));
    }

    // &result_vec[0..result_vec.len()]
    result_vec
}


fn get_next_x_minutes_avg_speed_new(x_min: usize, mut point_iter: Peekable<Skip<Windows<'_, Waypoint>>>) -> Vec<Option<f64>> {
    let current_points_time: OffsetDateTime = point_iter.peek().unwrap()[0].time.unwrap().into();
    // println!("{:?}", current_points_time);
    let mut array_vec: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_distance: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_time: Vec<Vec<OffsetDateTime>> = Vec::new();
    for i in 0..x_min {
        array_vec.push(Vec::new());
        array_vec_distance.push(Vec::new());
        array_vec_time.push(Vec::new());
    }


    for point in point_iter {
        let prev_point_time: OffsetDateTime = point[1].time.unwrap().into();
        // println!("{:?}", prev_point_time);
        let min_diff = ((current_points_time - prev_point_time).as_seconds_f64() / 60.0).abs().floor() as usize;
        if min_diff >= x_min {
            break;
        }
        array_vec_time[min_diff].push(point[1].time.unwrap().into());
        // let index = min_diff.floor() as usize;
        let speed = speed(&point[0], &point[1]);
        array_vec[min_diff].push(speed);

        let distance = distance_between_points(point[0].point(), point[0].elevation.unwrap(), point[1].point(), point[1].elevation.unwrap());
        array_vec_distance[min_diff].push(distance);


        // println!("{:?}", point);
        // println!("{:?}", min_diff);
    }
    // println!("{:#?}", array_vec);
    let mut result_vec: Vec<Option<f64>> = Vec::new();
    // println!("{:?}", array_vec_distance);

    for (num, i) in array_vec.into_iter().enumerate() {
        if i.len() == 0 {
            result_vec.push(None);
            // println!("oops");
            // println!("{:?}", array_vec_distance[num]);
        } else {
            let distance_sum: f64 = array_vec_distance[num].iter().sum();
            // println!("distance sum {}", distance_sum);
            // println!("distance len {}, time len {}",  array_vec_distance[num].len(), array_vec_time[num].len());
            let erm = array_vec_time[num][0] - array_vec_time[num][array_vec_time[num].len() - 1];
            let seconds = erm.as_seconds_f64().abs();
            if seconds == 0.0 {
                result_vec.push(None); // timestamps are the same bad calculation just skip
            } else {
                let speed_meters_per_second = distance_sum / erm.as_seconds_f64().abs();
                let speed_km_per_hour = speed_meters_per_second * METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR;
                // if speed_km_per_hour * 1.3 < average(&i[0..i.len()]) || speed_km_per_hour * 0.7 > average(&i[0..i.len()]) {
                //     println!("speed calcs differ {}, {}", speed_km_per_hour, average(&i[0..i.len()]))
                // }
                result_vec.push(Some(speed_km_per_hour));
            }
        }
    }

    // &result_vec[0..result_vec.len()]
    result_vec
}

fn get_next_x_minutes_avg_speed(x_min: usize, points: &Vec<Waypoint>, current_point_pos: usize) -> Vec<Option<f64>> {
    let current_points_time: OffsetDateTime = points[current_point_pos].time.unwrap().into();
    // println!("{:?}", current_points_time);
    let mut array_vec: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_distance: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_time: Vec<Vec<OffsetDateTime>> = Vec::new();
    for _ in 0..x_min {
        array_vec.push(Vec::new());
        array_vec_distance.push(Vec::new());
        array_vec_time.push(Vec::new());
    }


    for point in points.windows(2).skip(current_point_pos) {
        let prev_point_time: OffsetDateTime = point[1].time.unwrap().into();
        // println!("{:?}", prev_point_time);
        let min_diff = ((current_points_time - prev_point_time).as_seconds_f64() / 60.0).abs().floor() as usize;
        if min_diff >= x_min {
            break;
        }
        array_vec_time[min_diff].push(point[1].time.unwrap().into());
        // let index = min_diff.floor() as usize;
        let speed = speed(&point[0], &point[1]);
        array_vec[min_diff].push(speed);

        let distance = distance_between_points(point[0].point(), point[0].elevation.unwrap(), point[1].point(), point[1].elevation.unwrap());
        array_vec_distance[min_diff].push(distance);


        // println!("{:?}", point);
        // println!("{:?}", min_diff);
    }
    // println!("{:#?}", array_vec);
    let mut result_vec: Vec<Option<f64>> = Vec::new();
    // println!("{:?}", array_vec_distance);

    for (num, i) in array_vec.into_iter().enumerate() {
        if i.len() == 0 {
            result_vec.push(None);
            // println!("oops");
            // println!("{:?}", array_vec_distance[num]);
        } else {
            let distance_sum: f64 = array_vec_distance[num].iter().sum();
            // println!("distance sum {}", distance_sum);
            // println!("distance len {}, time len {}",  array_vec_distance[num].len(), array_vec_time[num].len());
            let erm = array_vec_time[num][0] - array_vec_time[num][array_vec_time[num].len() - 1];
            let seconds = erm.as_seconds_f64().abs();
            if seconds == 0.0 {
                result_vec.push(None); // timestamps are the same bad calculation just skip
            } else {
                let speed_meters_per_second = distance_sum / erm.as_seconds_f64().abs();
                let speed_km_per_hour = speed_meters_per_second * METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR;
                // if speed_km_per_hour * 1.3 < average(&i[0..i.len()]) || speed_km_per_hour * 0.7 > average(&i[0..i.len()]) {
                //     println!("speed calcs differ {}, {}", speed_km_per_hour, average(&i[0..i.len()]))
                // }
                result_vec.push(Some(speed_km_per_hour));
            }
        }
    }

    // &result_vec[0..result_vec.len()]
    result_vec
}


fn get_previous_x_2_minutes_avg_speed(x_min: usize, points: &Vec<Waypoint>, current_point_pos: usize) -> Vec<Option<f64>> {
    let current_points_time: OffsetDateTime = points[current_point_pos].time.unwrap().into();
    // println!("{:?}", current_points_time);
    let mut array_vec: Vec<Vec<f64>> = Vec::new();
    for i in 0..x_min {
        array_vec.push(Vec::new());
    }


    for point in points.windows(2).take(current_point_pos).rev() {
        let prev_point_time: OffsetDateTime = point[0].time.unwrap().into();
        // println!("{:?}", prev_point_time);
        let min_diff = ((current_points_time - prev_point_time).as_seconds_f64() / 60.0).floor() as usize;
        if min_diff * 2 >= x_min {
            break;
        }
        // let index = min_diff.floor() as usize;
        let speed = speed(&point[0], &point[1]);
        array_vec[min_diff / 2].push(speed);


        // println!("{:?}", point);
        // println!("{:?}", min_diff);
    }
    // println!("{:#?}", array_vec);
    let mut result_vec: Vec<Option<f64>> = Vec::new();
    for i in array_vec.into_iter() {
        if i.len() == 0 {
            result_vec.push(None);
        } else {
            result_vec.push(Some(average(&i[0..i.len()])));
        }
        // println!("MODE: {}", mode(&i));
        // println!("MEDIAN: {}", median(&mut i));
    }

    // &result_vec[0..result_vec.len()]
    result_vec
}

fn get_next_x_2_minutes_avg_speed(x_min: usize, points: &Vec<Waypoint>, current_point_pos: usize) -> Vec<Option<f64>> {
    let current_points_time: OffsetDateTime = points[current_point_pos].time.unwrap().into();
    // println!("{:?}", current_points_time);
    let mut array_vec: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_distance: Vec<Vec<f64>> = Vec::new();
    let mut array_vec_time: Vec<Vec<OffsetDateTime>> = Vec::new();
    for i in 0..x_min {
        array_vec.push(Vec::new());
        array_vec_distance.push(Vec::new());
        array_vec_time.push(Vec::new());
    }


    for point in points.windows(2).skip(current_point_pos) {
        let prev_point_time: OffsetDateTime = point[1].time.unwrap().into();
        // println!("{:?}", prev_point_time);
        let min_diff = ((current_points_time - prev_point_time).as_seconds_f64() / 60.0).abs().floor() as usize;
        if min_diff * 2 >= x_min {
            break;
        }
        array_vec_time[min_diff / 2].push(point[1].time.unwrap().into());
        // let index = min_diff.floor() as usize;
        let speed = speed(&point[0], &point[1]);
        array_vec[min_diff / 2].push(speed);

        let distance = distance_between_points(point[0].point(), point[0].elevation.unwrap(), point[1].point(), point[1].elevation.unwrap());
        array_vec_distance[min_diff / 2].push(distance);


        // println!("{:?}", point);
        // println!("{:?}", min_diff);
    }
    // println!("{:#?}", array_vec);
    let mut result_vec: Vec<Option<f64>> = Vec::new();
    // println!("{:?}", array_vec_distance);

    for (num, i) in array_vec.into_iter().enumerate() {
        if i.len() == 0 {
            result_vec.push(None);
            // println!("oops");
            // println!("{:?}", array_vec_distance[num]);
        } else {
            let distance_sum: f64 = array_vec_distance[num].iter().sum();
            // println!("distance sum {}", distance_sum);
            // println!("distance len {}, time len {}",  array_vec_distance[num].len(), array_vec_time[num].len());
            let erm = array_vec_time[num][0] - array_vec_time[num][array_vec_time[num].len() - 1];
            let seconds = erm.as_seconds_f64().abs();
            if seconds == 0.0 {
                result_vec.push(None); // timestamps are the same bad calculation just skip
            } else {
                let speed_meters_per_second = distance_sum / erm.as_seconds_f64().abs();
                let speed_km_per_hour = speed_meters_per_second * METERS_PER_SECOND_TO_KILOMETERS_PER_HOUR;
                // if speed_km_per_hour * 1.3 < average(&i[0..i.len()]) || speed_km_per_hour * 0.7 > average(&i[0..i.len()]) {
                //     println!("speed calcs differ {}, {}", speed_km_per_hour, average(&i[0..i.len()]))
                // }
                result_vec.push(Some(speed_km_per_hour));
            }
        }
    }

    // &result_vec[0..result_vec.len()]
    result_vec
}


fn average(numbers: &[f64]) -> f64 {
    numbers.iter().sum::<f64>() as f64 / numbers.len() as f64
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_load_csv_stock_file() {
        let file = File::open("./test_files/1.gpx").unwrap();
        let reader = BufReader::new(file);

        // read takes any io::Read and gives a Result<Gpx, Error>.
        let mut gpx: Gpx = read(reader).unwrap();
        let erm = get_next_x_minutes_avg_speed(5, &gpx.tracks[0].segments[0].points, 0);
        let erm_new = get_next_x_minutes_avg_speed_new(5, gpx.tracks[0].segments[0].points.windows(2).skip(0).peekable());

        assert_eq!(erm_new[0], Some(107.37588399905206));

        assert_eq!(erm.len(), erm_new.len());
        assert_eq!(erm[0], erm_new[0]);
        assert_eq!(erm[1], erm_new[1]);
        assert_eq!(erm[2], erm_new[2]);
        assert_eq!(erm[3], erm_new[3]);
        assert_eq!(erm[4], erm_new[4]);
    }
}*/

/// for now we are doing a super simple instantaneous speed from 1 to the next
/// maybe in future i can smooth data out by confirming there is no turn and increase data distance
pub fn get_avg_speed_from_array(array: &[PointWithMetadata]) -> Velocity {
    let total_seed = array
        .windows(2)
        .fold(Velocity::new::<kilometer_per_hour>(0.0), |total, window| {
            total + speed(&window[0], &window[1])
        });
    trace!("Array len {:?}", array.len());
    total_seed / (array.len() as f64)
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{TimeZone, Utc};
    use test_log::test;

    #[test]
    fn test_avg_speed_from_array() {
        // env_logger::init();

        let points: [PointWithMetadata; 4] = [
            PointWithMetadata {
                time: Utc.with_ymd_and_hms(2020, 1, 1, 0, 0, 0).unwrap().into(),
                lat: 0.0,
                lon: 0.0,
                ele: Some(0.0),
                s2_cell_id: None,
                recording_app: None,
                recording_device: None,
                metadata: Default::default(),
            },
            PointWithMetadata {
                time: Utc.with_ymd_and_hms(2020, 1, 1, 0, 1, 0).unwrap().into(),
                lat: 1.0,
                lon: 0.0,
                ele: Some(1.0),
                s2_cell_id: None,
                recording_app: None,
                recording_device: None,
                metadata: Default::default(),
            },
            PointWithMetadata {
                time: Utc.with_ymd_and_hms(2020, 1, 1, 0, 2, 0).unwrap().into(),
                lat: 0.0,
                lon: 0.0,
                ele: Some(0.0),
                s2_cell_id: None,
                recording_app: None,
                recording_device: None,
                metadata: Default::default(),
            },
            PointWithMetadata {
                time: Utc.with_ymd_and_hms(2020, 1, 1, 0, 2, 0).unwrap().into(),
                lat: 0.0,
                lon: 0.0,
                ele: Some(0.0),
                s2_cell_id: None,
                recording_app: None,
                recording_device: None,
                metadata: Default::default(),
            },
        ];

        // oki I see an issue when time hasnt changed introducing NaN I should look into that!
        let x = get_avg_speed_from_array(&points);
        assert_eq!(x, Velocity::new::<kilometer_per_hour>(3339.5847239329546));
    }
}
