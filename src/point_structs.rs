use crate::InfluxReadableGpsPoint;
use chrono::{DateTime, FixedOffset};
use std::collections::HashMap;
use std::hash::Hash;
use std::ops::{AddAssign, SubAssign};
use uom::si::f64::{Acceleration, Velocity};

#[derive(Debug)]
pub struct Kilometers(pub f64);
#[derive(Debug)]
pub struct Meters(pub f64);
#[derive(Debug)]
pub struct Ms(pub f64);

#[derive(Debug, Clone)]
pub struct PointWithMetadata {
    pub time: DateTime<FixedOffset>, // think its nano seconds from epoch
    pub lat: f64,
    pub lon: f64,
    pub ele: Option<f64>,
    pub s2_cell_id: Option<String>,
    pub recording_app: Option<String>,
    pub recording_device: Option<String>,
    pub metadata: Metadata,
}
#[derive(Debug, Default, Clone)]
pub struct Metadata {
    pub instantaneous_speed: Option<Velocity>, // based of previous point to this point
    pub instantaneous_acceleration: Option<Acceleration>, // based of speed of this point and speed of next point, my thought process was at this point I mist be accelerating to get to the next point at speed y
    pub sliding_speed_window_values: HashMap<SlidingWindow, SlidingWindowValue<Velocity>>, // its possible due to interpolation we wont have enough data to calculate an average
    pub sliding_acceleration_window_values:
        HashMap<SlidingWindow, SlidingWindowValue<Acceleration>>, // its possible due to interpolation we wont have enough data to calculate an average
    // really i should store multiple activitys and their confidence
    pub activitys: HashMap<Activity, CurrentWeight>, // todo add activating weight
    pub predicted_activity: Activity,
}

/// AKA confidence, A type to hold the accumulated value of the rules processed
/// The main thing that sets this apart from just an f64 is the InsufficientData which is used to
/// represent an unrecoverable weight/confidence, for convenience there is the AddAssign and
/// SubAssign, both of which are a no-op if the weight is InsufficientData
///
/// Todo implement CurrentWeight.into<Option> for added convenience methods
#[derive(Clone, Debug, PartialEq)]
pub enum CurrentWeight {
    Value(f64),
    InsufficientData,
}

impl AddAssign<f64> for CurrentWeight {
    fn add_assign(&mut self, rhs: f64) {
        match self {
            CurrentWeight::Value(ref mut value) => {
                *value += rhs;
            }
            CurrentWeight::InsufficientData => {
                // Noop if we have hit insufficient data in the past we cannot change the weight
            }
        }
    }
}

impl SubAssign<f64> for CurrentWeight {
    fn sub_assign(&mut self, rhs: f64) {
        match self {
            CurrentWeight::Value(ref mut value) => {
                *value -= rhs;
            }
            CurrentWeight::InsufficientData => {
                // Noop if we have hit insufficient data in the past we cannot change the weight
            }
        }
    }
}

/// todo implement into option
#[derive(Debug, Clone)]
pub enum SlidingWindowValue<T> {
    Value(T),
    InsufficientData,
}

impl Into<geo::Point<f64>> for &PointWithMetadata {
    fn into(self) -> geo::Point<f64> {
        geo::Point::new(self.lat, self.lon)
    }
}

impl From<InfluxReadableGpsPoint> for PointWithMetadata {
    fn from(value: InfluxReadableGpsPoint) -> Self {
        PointWithMetadata {
            time: value.time,
            lat: value.lat,
            lon: value.lon,
            ele: Some(value.ele),
            s2_cell_id: Some(value.s2_cell_id),

            recording_app: Some(value.recording_app),
            recording_device: Some(value.recording_device),
            metadata: Default::default(),
        }
    }
}

impl From<&InfluxReadableGpsPoint> for PointWithMetadata {
    fn from(value: &InfluxReadableGpsPoint) -> Self {
        PointWithMetadata {
            time: value.time,
            lat: value.lat,
            lon: value.lon,
            ele: Some(value.ele),
            s2_cell_id: Some(value.s2_cell_id.clone()),

            recording_app: Some(value.recording_app.clone()),
            recording_device: Some(value.recording_device.clone()),
            metadata: Default::default(),
        }
    }
}
/// Sliding window
///
/// Used for hashmap of avg speed te intention is you can calculate an avg speed for like
///
/// 5 min ahead of the point your on would be
/// SlidingWindow(period: 5min, offset: 0),
///
/// previous 5 min would be
/// SlidingWindow(period: 5min, offset: -5min),
///
/// middle 5 min would be
/// SlidingWindow(period: 5min, offset: -2min30sc)
///
/// This also makes it possible to look beyond current point so say i want to know the avg speed
/// over 1 min 5 min in the future (could be useful for catagorising buses
/// SlidingWindow(period: 1min, offset: 5min)
///
/// todo have a way to confirm period is non negative! as it shouldn't be allowed
#[derive(Hash, Debug, Eq, PartialEq, Clone)]
pub struct SlidingWindow {
    pub period: chrono::Duration,
    pub interpolate: InterpolateGPSPoints,
    pub offset: chrono::Duration,
}

/// decides if we should or souldnt interpolate between gps points
/// if no, then we want to allow for sme wiggle room so say we have a wiggle room of 10 seconds
/// and our closest gps point is less than that (say 5 seconds) accept it otherwise reject it
///
/// if we interpolate then we can pick the method by which we do
#[derive(Hash, Debug, Eq, PartialEq, Clone)]
pub enum InterpolateGPSPoints {
    No(WiggleRoom),
    Yes(InterpolationMethod),
}

#[derive(Hash, Debug, Eq, PartialEq, Clone)]
pub enum InterpolationMethod {
    Flat,
}
pub type WiggleRoom = chrono::Duration;

#[derive(Debug, Eq, Hash, PartialEq, Clone, Default)]
pub enum Activity {
    PhysicalActivity(PhysicalActivity),
    Transport(Transport),
    Nothing, // stood still/indoors
    #[default]
    Unknown,
}

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub enum Transport {
    Train,
    Car,
    Bus,
    Boat,
}

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
pub enum PhysicalActivity {
    Walk,
    // Run,
    Cycle,
    Rollerblade,
    Swim,
}

pub struct RuleSet {
    pub rules_with_weights: Vec<RuleWeight>,
    pub activity: Activity,
    pub activation_confidence: f64,
}

#[derive(Debug)]
pub struct RuleWeight {
    pub rule: Rule,
    /// the value added if rule matched
    pub weight: f64,
    /// The value subtracted if rule did not match
    pub negative_weight: f64,
    pub insuficent_data_action: InsuficentDataAction,
}

/// Ignore is eqivelent to subtract(0.0)
#[derive(Debug)]
pub enum InsuficentDataAction {
    SubtractValue(f64),
    Ignore,
    DelaireRuleFailed,
}

#[derive(Debug, Clone)]
pub enum RuleSlidingWindow {
    Speed(RuleSlidingWindowSpeed),
    Acceleration(RuleSlidingWindowAcceleration),
}

impl RuleSlidingWindow {
    pub(crate) fn get_offset(&self) -> chrono::Duration {
        self.get_window().offset
        // match self {
        //     RuleSlidingWindow::Speed(value) => value.window.offset,
        //     RuleSlidingWindow::Acceleration(value) => value.window.offset
        // }
    }

    pub(crate) fn get_period(&self) -> chrono::Duration {
        self.get_window().period
        // match self {
        //     RuleSlidingWindow::Speed(value) => value.window.offset,
        //     RuleSlidingWindow::Acceleration(value) => value.window.offset
        // }
    }

    fn get_window(&self) -> &SlidingWindow {
        match self {
            RuleSlidingWindow::Speed(value) => &value.window,
            RuleSlidingWindow::Acceleration(value) => &value.window,
        }
    }
}

#[derive(Debug, Clone)]
pub struct RuleSlidingWindowSpeed {
    pub window: SlidingWindow,
    pub expected_value: ExpectedValue<Velocity>,
}

#[derive(Debug, Clone)]
pub struct RuleSlidingWindowAcceleration {
    pub window: SlidingWindow,
    pub expected_value: ExpectedValue<Acceleration>,
}

#[derive(Debug, Clone)]
pub enum RuleInstantaneous {
    RuleInstantaneousSpeed(ExpectedValue<Velocity>),
    RuleInstantaneousAcceleration(ExpectedValue<Acceleration>),
}

#[derive(Debug)]
pub enum Rule {
    Instantaneous(RuleInstantaneous),
    SlidingWindow(RuleSlidingWindow),
}

// impl RuleSpeedWindow {
//     pub fn as_sw_option_ref(&self) -> Option<&SlidingWindow> {
//         match self {
//             RuleSpeedWindow::Instantaneous => None,
//             RuleSpeedWindow::SlidingWindow(sw) => Some(sw),
//         }
//     }
// }

/// The value doesnt have a type, the type kmp/speed/acceleration is all contained in the sliding
/// window type
#[derive(Debug, Clone)]
pub enum ExpectedValue<T> {
    GreaterThan(T),
    EqualTo(T),
    LessThan(T),
}
