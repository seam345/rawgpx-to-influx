use crate::point_structs::{
    Activity, CurrentWeight, ExpectedValue, InsuficentDataAction, PointWithMetadata, Rule,
    RuleInstantaneous, RuleSet, RuleSlidingWindow, SlidingWindowValue,
};
use log::trace;
use std::collections::HashMap;

// oki this function wont work as i need data over time and i have a single point... or! i could before
// this function pull out all sliding windows in the rules and calculate them so I only operate
// on the individual points at this point.... hmmm oki that would mean this should return a result
// erroring out if the required data is missing
// this has all shifted so i can actually compute with or without data and update the point

// oh new plan is to allow confirmation that i filled in every point I should at least mark points I
// didnt compute as insuficent data before allowing this method to have a clear fail state
pub fn process_rules_on_point(point: &mut PointWithMetadata, rules: &HashMap<Activity, RuleSet>) {
    // let weight_result: Vec<f64> = Vec::with_capacity(rules.len());
    // rules.iter().map(|rule|)
    for (_, rule) in rules {
        for rules_with_weight in &rule.rules_with_weights {
            match &rules_with_weight.rule {
                Rule::Instantaneous(RuleInstantaneous::RuleInstantaneousSpeed(expected_value)) => {
                    match &point.metadata.instantaneous_speed {
                        None => {
                            todo!() // here would be missing computed data and should give an error log or something
                        }
                        Some(value) => match expected_value {
                            ExpectedValue::GreaterThan(expected_value) => {
                                if *value > *expected_value {
                                    point
                                        .metadata
                                        .activitys
                                        .entry(rule.activity.clone())
                                        .and_modify(|current_weight| {
                                            *current_weight += rules_with_weight.weight
                                        })
                                        .or_insert(CurrentWeight::Value(rules_with_weight.weight));
                                } else {
                                    point
                                        .metadata
                                        .activitys
                                        .entry(rule.activity.clone())
                                        .and_modify(|current_weight| {
                                            *current_weight -= rules_with_weight.negative_weight
                                        })
                                        .or_insert(CurrentWeight::Value(
                                            0.0 - rules_with_weight.negative_weight,
                                        ));
                                }
                            }
                            ExpectedValue::EqualTo(expected_value) => {
                                if *value == *expected_value {
                                    point
                                        .metadata
                                        .activitys
                                        .entry(rule.activity.clone())
                                        .and_modify(|current_weight| {
                                            *current_weight += rules_with_weight.weight
                                        })
                                        .or_insert(CurrentWeight::Value(rules_with_weight.weight));
                                } else {
                                    point
                                        .metadata
                                        .activitys
                                        .entry(rule.activity.clone())
                                        .and_modify(|current_weight| {
                                            *current_weight -= rules_with_weight.negative_weight
                                        })
                                        .or_insert(CurrentWeight::Value(
                                            0.0 - rules_with_weight.negative_weight,
                                        ));
                                }
                            }
                            ExpectedValue::LessThan(expected_value) => {
                                if *value < *expected_value {
                                    point
                                        .metadata
                                        .activitys
                                        .entry(rule.activity.clone())
                                        .and_modify(|current_weight| {
                                            *current_weight += rules_with_weight.weight
                                        })
                                        .or_insert(CurrentWeight::Value(rules_with_weight.weight));
                                } else {
                                    point
                                        .metadata
                                        .activitys
                                        .entry(rule.activity.clone())
                                        .and_modify(|current_weight| {
                                            *current_weight -= rules_with_weight.negative_weight
                                        })
                                        .or_insert(CurrentWeight::Value(
                                            0.0 - rules_with_weight.negative_weight,
                                        ));
                                }
                            }
                        },
                    }
                }
                Rule::Instantaneous(RuleInstantaneous::RuleInstantaneousAcceleration(_)) => {
                    todo!()
                }
                Rule::SlidingWindow(RuleSlidingWindow::Speed(sliding_window_rule)) => {
                    match point
                        .metadata
                        .sliding_speed_window_values
                        .get(&sliding_window_rule.window)
                    {
                        None => {
                            todo!() // here would be missing computed data and should give an error log or something
                        }
                        Some(SlidingWindowValue::Value(value)) => {
                            match &sliding_window_rule.expected_value {
                                ExpectedValue::GreaterThan(expected_value) => {
                                    if *value > *expected_value {
                                        point
                                            .metadata
                                            .activitys
                                            .entry(rule.activity.clone())
                                            .and_modify(|current_weight| {
                                                *current_weight += rules_with_weight.weight
                                            })
                                            .or_insert(CurrentWeight::Value(
                                                rules_with_weight.weight,
                                            ));
                                    } else {
                                        point
                                            .metadata
                                            .activitys
                                            .entry(rule.activity.clone())
                                            .and_modify(|current_weight| {
                                                *current_weight -= rules_with_weight.negative_weight
                                            })
                                            .or_insert(CurrentWeight::Value(
                                                0.0 - rules_with_weight.negative_weight,
                                            ));
                                    }
                                }
                                ExpectedValue::EqualTo(expected_value) => {
                                    if *value == *expected_value {
                                        point
                                            .metadata
                                            .activitys
                                            .entry(rule.activity.clone())
                                            .and_modify(|current_weight| {
                                                *current_weight += rules_with_weight.weight
                                            })
                                            .or_insert(CurrentWeight::Value(
                                                rules_with_weight.weight,
                                            ));
                                    } else {
                                        point
                                            .metadata
                                            .activitys
                                            .entry(rule.activity.clone())
                                            .and_modify(|current_weight| {
                                                *current_weight -= rules_with_weight.negative_weight
                                            })
                                            .or_insert(CurrentWeight::Value(
                                                0.0 - rules_with_weight.negative_weight,
                                            ));
                                    }
                                }
                                ExpectedValue::LessThan(expected_value) => {
                                    if *value < *expected_value {
                                        point
                                            .metadata
                                            .activitys
                                            .entry(rule.activity.clone())
                                            .and_modify(|current_weight| {
                                                *current_weight += rules_with_weight.weight
                                            })
                                            .or_insert(CurrentWeight::Value(
                                                rules_with_weight.weight,
                                            ));
                                    } else {
                                        point
                                            .metadata
                                            .activitys
                                            .entry(rule.activity.clone())
                                            .and_modify(|current_weight| {
                                                *current_weight -= rules_with_weight.negative_weight
                                            })
                                            .or_insert(CurrentWeight::Value(
                                                0.0 - rules_with_weight.negative_weight,
                                            ));
                                    }
                                }
                            }
                        }
                        Some(SlidingWindowValue::InsufficientData) => {
                            match rules_with_weight.insuficent_data_action {
                                InsuficentDataAction::SubtractValue(_) => {
                                    todo!()
                                }
                                InsuficentDataAction::Ignore => {}
                                InsuficentDataAction::DelaireRuleFailed => {
                                    point
                                        .metadata
                                        .activitys
                                        .entry(rule.activity.clone())
                                        .and_modify(|current_weight| {
                                            *current_weight = CurrentWeight::InsufficientData
                                        })
                                        .or_insert(CurrentWeight::InsufficientData);
                                }
                            }
                        }
                    }
                }
                Rule::SlidingWindow(RuleSlidingWindow::Acceleration(_)) => {
                    todo!()
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::point_structs::{
        InterpolateGPSPoints, Metadata, RuleSlidingWindowSpeed, RuleWeight, SlidingWindow,
    };
    use chrono::{TimeZone, Utc};
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    use crate::point_structs::Activity::PhysicalActivity;
    use crate::point_structs::PhysicalActivity::Walk;
    use test_log::test;
    use uom::si::f64::Velocity;
    use uom::si::velocity::kilometer_per_hour;

    #[test]
    fn test_avg_speed_from_array() {
        let mut point = PointWithMetadata {
            time: Utc.with_ymd_and_hms(2020, 1, 1, 0, 0, 0).unwrap().into(),
            lat: 0.0,
            lon: 0.0,
            ele: Some(0.0),
            s2_cell_id: None,
            recording_app: None,
            recording_device: None,
            metadata: Metadata {
                instantaneous_speed: Some(Velocity::new::<kilometer_per_hour>(6.0)),
                instantaneous_acceleration: None,
                sliding_speed_window_values: HashMap::from([(
                    SlidingWindow {
                        period: chrono::Duration::seconds(300),
                        interpolate: InterpolateGPSPoints::No(chrono::Duration::seconds(5)),
                        offset: chrono::Duration::seconds(-150),
                    },
                    SlidingWindowValue::Value(Velocity::new::<kilometer_per_hour>(6.0)),
                )]),
                sliding_acceleration_window_values: Default::default(),
                activitys: Default::default(),
                predicted_activity: Activity::Nothing,
            },
        };

        let mut rule_sets: HashMap<Activity, RuleSet> = HashMap::new();
        rule_sets.insert(
            Activity::PhysicalActivity(Walk),
            RuleSet {
                rules_with_weights: vec![
                    RuleWeight {
                        rule: Rule::Instantaneous(RuleInstantaneous::RuleInstantaneousSpeed(
                            ExpectedValue::LessThan(Velocity::new::<kilometer_per_hour>(8.0)),
                        )),
                        weight: 1.0,
                        negative_weight: 0.0,
                        insuficent_data_action: InsuficentDataAction::Ignore,
                    },
                    RuleWeight {
                        rule: Rule::Instantaneous(RuleInstantaneous::RuleInstantaneousSpeed(
                            ExpectedValue::GreaterThan(Velocity::new::<kilometer_per_hour>(1.0)),
                        )),
                        weight: 1.0,
                        negative_weight: 0.0,
                        insuficent_data_action: InsuficentDataAction::Ignore,
                    },
                    RuleWeight {
                        rule: Rule::SlidingWindow(RuleSlidingWindow::Speed(
                            RuleSlidingWindowSpeed {
                                window: SlidingWindow {
                                    period: chrono::Duration::seconds(300),
                                    interpolate: InterpolateGPSPoints::No(
                                        chrono::Duration::seconds(5),
                                    ),
                                    offset: chrono::Duration::seconds(-150),
                                },
                                expected_value: ExpectedValue::LessThan(Velocity::new::<
                                    kilometer_per_hour,
                                >(
                                    8.0
                                )),
                            },
                        )),

                        weight: 1.0,
                        negative_weight: 0.0,
                        insuficent_data_action: InsuficentDataAction::Ignore,
                    },
                    RuleWeight {
                        rule: Rule::SlidingWindow(RuleSlidingWindow::Speed(
                            RuleSlidingWindowSpeed {
                                window: SlidingWindow {
                                    period: chrono::Duration::seconds(300),
                                    interpolate: InterpolateGPSPoints::No(
                                        chrono::Duration::seconds(5),
                                    ),
                                    offset: chrono::Duration::seconds(-150),
                                },
                                expected_value: ExpectedValue::GreaterThan(Velocity::new::<
                                    kilometer_per_hour,
                                >(
                                    1.0
                                )),
                            },
                        )),
                        weight: 1.0,
                        negative_weight: 0.0,
                        insuficent_data_action: InsuficentDataAction::Ignore,
                    },
                ],
                activity: Activity::PhysicalActivity(Walk),
                activation_confidence: 2.0,
            },
        );

        process_rules_on_point(&mut point, &rule_sets);
        assert_eq!(
            point.metadata.activitys.get(&PhysicalActivity(Walk)),
            Some(&CurrentWeight::Value(4.0))
        );
        // assert_eq!(point.predicted_activity, PhysicalActivity(Walk));
    }
}

pub fn process_rules(
    mut points: ProcessedPoints,
    rules: HashMap<Activity, RuleSet>,
) -> ProcessedPoints {
    trace!("Process rules");
    for (index, working_index) in points.workable_points.iter().enumerate() {
        if index % 100 == 0 {
            println!(
                "at index \t {index}\t  of {} ",
                points.workable_points.len()
            );
        }
        process_rules_on_point(&mut points.all_points[*working_index], &rules);
    }

    trace!("calculate predicted activity");
    for point in &mut points.all_points {
        for (activity, current_weight) in &point.metadata.activitys {
            match current_weight {
                CurrentWeight::Value(current_weight_value) => {
                    if *current_weight_value > rules.get(activity).unwrap().activation_confidence {
                        if point.metadata.predicted_activity != Activity::Unknown {
                            todo!()
                        }
                        point.metadata.predicted_activity = activity.clone();
                    }
                }
                CurrentWeight::InsufficientData => {}
            }
        }
    }
    points
}

pub struct ProcessedPoints {
    pub(crate) all_points: Vec<PointWithMetadata>,
    pub(crate) workable_points: Vec<usize>,
}
