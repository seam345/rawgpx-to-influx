use crate::point_helpers;
use crate::point_structs::{
    Activity, InterpolateGPSPoints, PointWithMetadata, Rule, RuleSet, RuleSlidingWindow,
    SlidingWindow, SlidingWindowValue, WiggleRoom,
};
use crate::rule_processors::ProcessedPoints;
use log::trace;
use std::collections::HashMap;

/// intention here is to compute all sliding windows in the rule set as a preable to processing the rules
/// this i done so we can process the rules further down the pipe by only looking at a single point instead
/// of arbitery windows of points, simplifying things
///
/// maybe rename to `compute_metadata`
pub fn process_sliding_windows_on_data(
    mut points: Vec<PointWithMetadata>,
    rules: &HashMap<Activity, RuleSet>,
) -> ProcessedPoints {
    // let all_sliding_windows : HashSet<SlidingWindow> = rules
    //     .iter()
    //     .flat_map(|rs| rs.rules_with_weights)
    //     .map(|rw| rw.rule.speed_window.is_avg())
    //     .flatten()
    //     .map(|sw| sw.hash())
    //     .collect();

    trace!("Extract all sliding windows in rules as an iterator");
    // todo consider confirming unique
    let all_sliding_windows = rules
        .iter()
        .flat_map(|(_, rule_set)| &rule_set.rules_with_weights)
        .flat_map(|rule_weight| match &rule_weight.rule {
            Rule::SlidingWindow(sliding_window) => Some(sliding_window),
            _ => None,
        });

    trace!("Calculate the fruthest time back we look and time forwards");
    // this is used to then shrink the data to only work on points that we can get the full story about
    // todo break out the following into a function and add unit tests to confirm it works as expected

    let (max_back, max_forwards) = all_sliding_windows.clone().fold(
        (chrono::Duration::seconds(0), chrono::Duration::seconds(0)),
        |(mut max_back, mut max_forwards), window| {
            max_back = max_back.min(window.get_offset());
            max_forwards = max_forwards.max(window.get_offset() + window.get_period());

            (max_back, max_forwards)
        },
    );

    trace!("construct ProcessedPoints");
    let first_point_time = points[0].time;
    let earlest_time = first_point_time - max_back;
    let last_point_time = points[points.len() - 1].time;
    let latest_time = last_point_time - max_forwards;
    let workable_points: Vec<usize> = points
        .iter_mut()
        .enumerate()
        .filter(|(_, point)| point.time > earlest_time && point.time < latest_time)
        .map(|(index, _)| index)
        .collect();

    let processed_points = ProcessedPoints {
        all_points: points,
        workable_points,
    };

    trace!("calculate all sliding windows and add them to metadata points");

    // for sliding_window in all_sliding_windows {
    //     process_sliding_window_on_data(processed_points, sliding_window);
    // }

    let mut processed_points = all_sliding_windows
        .fold(processed_points, |points, sliding_window| {
            process_sliding_window_on_data(points, sliding_window)
        });

    for working_point in &processed_points.workable_points {
        let working_point = *working_point;
        let speed = point_helpers::speed(
            &processed_points.all_points[working_point],
            &processed_points.all_points[working_point + 1],
        );
        processed_points.all_points[working_point]
            .metadata
            .instantaneous_speed = Some(speed);
    }

    processed_points
}

// todo atm this method can panic.... debate if that's ok
fn get_point_window<'a>(
    index: usize,
    points: &'a [PointWithMetadata],
    window: &SlidingWindow,
) -> Option<&'a [PointWithMetadata]> {
    // trace!("get_point_window");
    let mut min_index = index;
    let mut max_index = index;

    while points[min_index].time > points[index].time + window.offset {
        min_index -= 1;
    }
    while points[max_index].time < points[index].time + (window.offset + window.period) {
        max_index += 1;
    }

    // todo work out interpolation
    match window.interpolate {
        InterpolateGPSPoints::No(wiggle_room) => {
            if !lower_time_in_wiggle_room(
                &points[min_index],
                &points[index],
                window.offset,
                wiggle_room,
            ) {
                return None;
            }
            if !upper_time_in_wiggle_room(
                &points[max_index],
                &points[index],
                window.offset,
                window.period,
                wiggle_room,
            ) {
                return None;
            }
        }
        InterpolateGPSPoints::Yes(_) => {
            todo!()
        }
    }

    Some(&points[min_index..max_index])
}

/// this function confirms if the point is within acceptable bounds (wiggle room) of the window (period/offset)
fn lower_time_in_wiggle_room(
    point_in_question: &PointWithMetadata,
    origin_point: &PointWithMetadata,
    // period: chrono::Duration,
    offset: chrono::Duration,
    wiggle_room: WiggleRoom,
) -> bool {
    /*if*/
    point_in_question.time >= origin_point.time + offset - wiggle_room
        && point_in_question.time <= origin_point.time + offset + wiggle_room
    // {
    //     return true
    // }
    // false
}

fn upper_time_in_wiggle_room(
    point_in_question: &PointWithMetadata,
    origin_point: &PointWithMetadata,
    period: chrono::Duration,
    offset: chrono::Duration,
    wiggle_room: WiggleRoom,
) -> bool {
    /*if*/
    point_in_question.time >= origin_point.time + (offset + period) - wiggle_room
        && point_in_question.time <= origin_point.time + (offset + period) + wiggle_room
    // {
    //     return true
    // }
    // false
}

/// new idea no can query for more data I shall shrink the data to allow all windows to be able to be processed!!!
fn process_sliding_window_on_data(
    mut points: ProcessedPoints,
    window: &RuleSlidingWindow,
) -> ProcessedPoints {
    trace!("process sliding window on data");
    'calc: for (index, working_point) in points.workable_points.iter().enumerate() {
        if index % 100 == 0 {
            println!("at index \t{index}\t in {}", &points.workable_points.len());
        }
        let working_point = *working_point;

        match window {
            RuleSlidingWindow::Speed(rule_window) => {
                let Some(window_points) =
                    get_point_window(working_point, &points.all_points, &rule_window.window)
                else {
                    points.all_points[working_point]
                        .metadata
                        .sliding_speed_window_values
                        .insert(
                            rule_window.window.clone(),
                            SlidingWindowValue::InsufficientData,
                        );
                    continue 'calc;
                };

                let avg_speed = point_helpers::get_avg_speed_from_array(&window_points);
                points.all_points[working_point]
                    .metadata
                    .sliding_speed_window_values
                    .insert(
                        rule_window.window.clone(),
                        SlidingWindowValue::Value(avg_speed),
                    );
            }
            RuleSlidingWindow::Acceleration(_) => {
                todo!()
            }
        }
    }
    points
}
